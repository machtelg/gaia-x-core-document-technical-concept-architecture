-- The GAIA-X catalogue has a "dual view" on Self-Descriptions:
-- 1. Self-Descriptions as self-contained JSON-LD files
-- 2. Many Self-Descriptions aggregated into a Self-Description graph.
--
-- The following is a database schema to hold the raw JSON-LD Self-Descriptions.

CREATE TABLE statuscodes (
    id            INTEGER       PRIMARY KEY,
    status        VARCHAR(20)   NOT NULL UNIQUE
);

INSERT INTO statuscodes (id, status)
VALUES
  (1, 'active'),
  (2, 'deprecated'),  -- deprecated: a new version of the self-description exists
  (3, 'eol'),         -- eol: end of lifetime (assets can continue to run, but no more provisioning)
  (4, 'revoked');     -- revoked: any of the signing parties or a trusted party revoked

CREATE TABLE self_descriptions (
    hash          CHAR(64)      PRIMARY KEY, -- Hash of the JSON-LD self-description
    subject       VARCHAR       not null,    -- GAIA-X identifier of the subject (@id) of the JSON-LD self-description
    validators    VARCHAR[],                 -- GAIA-X identifiers of the participants who validated the self-description
    issued        DATE          not null,    -- When was the self-description issue?
    received      DATE          not null,    -- When was the self-description first received by this catalogue?
    jsonld        VARCHAR       not null,    -- The raw JSON-LD matching the hash
	status        INTEGER       NOT NULL DEFAULT 1, -- Status of the Self-Description
	CONSTRAINT fk_statuscodes FOREIGN KEY (status) references statuscodes (id) -- Only valid status are allowed
);

-- Open Discussion Points:
-- status and versioning are security-relevant
-- what about self-descriptions that contradict each other?
-- a self-description that is an update needs to contain a "deprecates" field which other self-descriptions are deprecated by it.