# Federation Services

The *Federation Services* are necessary to enable a Federation of
infrastructure and data, provided with open source reference
implementation. This will open up technology where applicable, while
existing *Certifications* and standards for *Accreditation* will be
recognized.

Details about the operationalization of the *Federation Service* will be
outlined in the upcoming Federation Services documents. Details about
the role of *Federation Services* for *Ecosystems* are elaborated in section
[Gaia-X Ecosystems](ecosystem.md), with an overview shown in the figure below.

-   The *[Federated Catalogue](#federated-catalogue)* constitutes an index
    repository of Gaia-X Self-Descriptions to enable the discovery and
    selection of Providers and their Service Offerings. The
    Self-Description as expression of properties and Claims of
    Participants and Assets represents a key element for transparency
    and trust in Gaia-X.

-   *[Identity and Trust](#identity-and-trust)* covers authentication and
    authorization, credential management, decentralized Identity management
    as well as the verification of analogue credentials.

-   *[Data Sovereignty Services](#data-sovereignty-services)* enable the sovereign data
    exchange of Participants by providing a Data Agreement Service and a
    Data Logging Service to enable the enforcement of Policies. Further,
    usage constraints for data exchange can be expressed by Provider
    Policies as part of the Self-Description.

-   *[Compliance](#compliance)* includes mechanisms to ensure a
    Participant's adherence to the Policy Rules in areas such as
    security, privacy transparency and interoperability during
    onboarding and service delivery.

-   *[Gaia-X Portals and API](#gaia-x-portals-and-api)* will support onboarding and
    Accreditation of Participants, demonstrate service discovery,
    orchestration and provisioning of sample services.

![](media/image4.png)
*Gaia-X Federation Services and Portal as covered in the Architecture Document*

## Federated Catalogue

Self-Descriptions intended for public usage can be published in a
Catalogue where they can be found by potential Consumers. The Providers
decide in a self-sovereign manner which information they want to make
public in a Catalogue and which information they only want to share
privately. The goal of Catalogues is to enable Consumers to find
best-matching offerings and to monitor for relevant changes of the
offerings.

A Catalogue contains two types of databases for Self-Descriptions:
The Self-Description Storage contains the published Self-Description
files in the JSON-LD format together with additional lifecycle metadata.
The Self-Description Graph imports the Self-Descriptions from the Self-Description
Storage into an aggregate data structure. The individual Self-Descriptions
can reference between each other. The Self-Description Graph is the basis
for advanced query mechanisms that take the references between Self-Descriptions
into consideration.


Cross-referencing is enabled by unique Identifiers as described in [Identity and Trust](federation_service.md#identity-and-trust).
While uniqueness means that Identifiers do not refer to more than one entity, 
there can be several Identifiers referring to the same entity. 
A Catalogue should not use multiple Identifiers for the same entity.

The system of Federated Catalogues comprises a top-level Catalogue
operated by the Gaia-X, European Association for Data and Cloud, AISBL
as well as Ecosystem-specific Catalogues (e.g., for the healthcare
domain) and even company-internal Catalogues with private
Self-Descriptions to be used only internally. Self-Descriptions in a
Catalogue are either loaded directly into a Catalogue or exchanged from
another Catalogue by an inter-Catalogue synchronization interface.

Since Self-Descriptions are protected by cryptographic signatures, they
are immutable and cannot be changed once published. This implies that after changes to a subset of attributes are made, e.g. signing by a certification authority, the Participant as the Self-Description issuer has to sign the Self-Description again.
The lifecycle state of a Self-Description is described in additional metadata. 
There are four possible states for the Self-Description lifecycle. The default
state is "active". The other states are terminal, i.e., no further state
transitions follow upon them:

-   Active

-   End-of-Life (after a timeout date, e.g., the expiry of a
    cryptographic signature)

-   Deprecated (by a newer Self-Description)

-   Revoked (by the original issuer or a trusted party, e.g., because it
    contained wrong or fraudulent information)

The Catalogues provide access to the raw Self-Descriptions that are
currently loaded including the lifecycle metadata. This allows Consumers
to verify the Self-Descriptions and the cryptographic proofs contained
in them in a self-sovereign manner.

The Self-Description Graph contains the information imported from the
Self-Descriptions that are known to a Catalogue and in an "active"
lifecycle state. The Self-Description Graph allows for complex queries
across Self-Descriptions.

To present search results objectively and without discrimination,
compliant Catalogues use a query engine with no internal ranking of results.
Users can define filter- and sort-criteria in their queries. But if some results have no unique
ordering according to the defined sort-criteria, they are randomized.
The random seed for the search result ordering is set on a per-session basis so that the
query results are repeatable within a session with a Catalogue.

In a private Catalogue, the authentication information can be used to
allow a user to upload new Self-Descriptions and/or change the lifecycle
state of existing ones. In a public Catalogue, the cryptographic
signatures of the Self-Descriptions are checked if its issuer is the
owner of its subject. If that is the case, the Self-Description is
accepted by the Catalogue. Hence, Self-Descriptions can be communicated
to the Catalogue by third parties, as the trust verification is
independent of the distribution mechanism. Self-Descriptions can be
marked by the issuer as "non-public" to prevent them from being copied
public Catalogue by a third party that received the Self-Description
JSON-LD file over a private channel.

A Visitor is an anonymous user accessing a Catalogue without a known
account for session. Every Non-Visitor user (see Principal in section
3.2) interacts with a Catalogue REST API in the context of a session.
Another option to interact with a Catalogue is to use a GUI frontend
(e.g. a Gaia-X Portal or a custom GUI implementation) that uses a
Catalogue REST API in the background. The interaction between a
Catalogue and its GUI frontend is based on an authenticated session for
the individual user of the GUI frontend.

### Self-Description

Gaia-X Self-Descriptions express characteristics of Assets, Resources,
Service Offerings and Participants and are tied to their respective
Identifier. Providers are responsible for the creation of their Asset's
or Resource's Self-Description. In addition to self-declared Claims made
by Participants about themselves or about the Service Offering provided
by them, a Self-Description may comprise Credentials issued and signed
by trusted parties. Such Credentials include Claims about the Provider
or Asset/Resource, which have been asserted by the issuer.

Self-Descriptions in combination with trustworthy verification
mechanisms empower Participants in their decision-making process.
Specifically, Self-Descriptions can be used for:

-   Discovery and composition of Service Offerings in a Catalogue

-   Tool-assisted evaluation, selection, integration and orchestration
    of Service Instances comprising Assets and Resources

-   Enforcement, continuous validation and trust monitoring together
    with Usage Policies

-   Negotiation of contractual terms concerning Assets and Resources of
    a Service Offering and Participants

Gaia-X Self-Descriptions are characterized by the following properties:

-   Machine-readable and machine-interpretable

-   Technology-agnostic

-   Adhering to a generalized schema with an expressive semantics and
    validation rules

-   Interoperable, following standards in terms of format, structure,
    and included expressions (semantics)

-   Flexible, extensible and future-proof in that new properties can be
    added

-   Navigable and can be referenced from anywhere in a unique, decentralized fashion

-   Accompanied by statements of proof (e.g., certificates and
    signatures), making them trustworthy by providing cryptographically
    secure verifiable information

A possible exchange format for Self-Descriptions can be the JSON-LD
format. JSON-LD uses JSON encoding to represent subject-predicate-object
triples according to the W3C Resource Description Framework (RDF).

The Self-Description of one entity may refer to another entity by its
Identifier. These relations form a graph with typed edges, which is
called the Self-Description Graph. The Catalogues implement a query
algorithm on top of the Self-Description Graph. Furthermore,
Certification aspects and Usage Policies can be expressed and checked
based on the Self-Description Graph that cannot be gained from
individual Self-Descriptions. For example, a Consumer could use
Catalogue Services to require that a Service Instance cannot depend on
other Service Instances that are deployed on Nodes outside a
Consumer-specified list of acceptable countries.

A Self-Description contains the Identifier of the Asset, Resource or
Participant, metadata and one or more Credentials as shown in the figure below.
A Credential contains one or more Claims, comprised of subjects,
properties and values. The metadata of each Credential includes issuing
timestamps, expiry dates, issuer references and so forth. Each
Credential can have a cryptographic signature, wherein trusted parties
confirm the contained Claims. Claims may follow the same
subject--property--object structure of the data model. The W3C
Verifiable Credentials Data Model[^11] is the technical standard
to express Credentials and Claims on top of JSON-LD[^12].

[^11]: W3C. Verifiable Credentials Data Model 1.0: Expressing verifiable information on the Web [W3C Recommendation 19 November 2019]. <https://www.w3.org/TR/vc-data-model/>
[^12]: W3C. JSON-LD 1.1: A JSON-based Serialization for Linked Data [W3C Recommendation 16 July 2020]. <https://www.w3.org/TR/json-ld11/>

```mermaid
graph LR
    subgraph Self-Description
        metadata1[Metadata]
        vc1[Verifiable Credential - 1..*]
        proof1[Proof Info - 1..*]
    end

    subgraph Verifiable Credential
        metadata2[Metadata]
        claim[Claim - 1..*]
        proof2[Proof Info - 1..*]
    end

    vc1 -- 1 .. * --> claim

    %% classDef attribute fill:#f9f,stroke:#333,stroke-width:4px,rx:50%,ry:50%; %%
    classDef attribute stroke-width:2px,rx:50%,ry:50%;
    class iso,date attribute;
```
*Self-Description assembly model*
<!-- ![](media/image5.png) -->

The generic data model for Claims is powerful and can be used to express
a large variety of statements. Individual Claims can be merged together
to express a graph of information about an Asset/Resource (subject). For
example, a Node complying with ISO 27001 is shown in the figure below.

```mermaid
graph TB
    subgraph Credential Graph
        cred123[Credential 123] -- credentialSubject --> node[Node]
        cred123 -- issuer --> duv[DUV]
        node -- hasCertificate --> iso(ISO 27001)
    end

    subgraph Proof Graph
        sig456[Signature 456] -- creator --> jane[Jane Doe]
        sig456 -- created --> date(2021-03-01 14:01:46)
    end

    duv -- proof --> sig456

    %% classDef attribute fill:#f9f,stroke:#333,stroke-width:4px,rx:50%,ry:50%; %%
    classDef attribute stroke-width:2px,rx:50%,ry:50%;
    class iso,date attribute;
```
*Linked Claim statements as a graph representation*
<!-- ![](media/image6.png) -->

To foster interoperability, Self-Description schemas with optional and
mandatory properties and relations are defined. A Self-Description has
to state which schemas are used in its metadata. Only properties and
relations defined in these schemas must be used. A Self-Description
schema corresponds to a class in RDF. The Self-Description schemas form
an extensible class hierarchy with inheritance of properties and
relations.

The Gaia-X Federation Services specification describes how core
Self-Description schemas based on the conceptual model are created and
maintained. Individual Gaia-X Ecosystems can extend the schema hierarchy
for their application domain.[^13] Such extensions must make an explicit
reference to the organization that is responsible for the development
and maintenance of the extension.

[^13]: This is in analogy to, e.g., how DCAT-AP specifies the application of DCAT for data portals in Europe; European Commission Semantic Interoperability Community. DCAT Application Profile for data portals in Europe. <https://joinup.ec.europa.eu/collection/semantic-interoperability-community-semic/solution/dcat-application-profile-data-portals-europe>

![](media/image7.png)
*Schematic inheritance relations and properties for the top-level Self-Description*

The Self-Description Schemas can follow the Linked Data best
practices[^14] making the W3C Semantic Web family[^15] a possible
standard to be built upon to enable broad adoption and tooling.

[^14]: Berners-Lee, T. (2009). Linked Data. W3C. <https://www.w3.org/DesignIssues/LinkedData>
[^15]: W3C. (2015). Semantic Web. <https://www.w3.org/standards/semanticweb/>

Gaia-X aims at building upon existing schemas, preferably such that have
been standardized or at least widely adopted[^16] to get a common
understanding of the meaning and purpose of any property and Claim
statement. Examples of attribute categories per Self-Description in
Gaia-X are discussed in the Appendix [A1](appendix.md#a1).

[^16]: Examples include the W3C Organization Ontology (<https://www.w3.org/TR/vocab-org/>), the community-maintained schema.org vocabulary (<https://schema.org/>), the W3C Data Catalog Vocabulary DCAT (<https://www.w3.org/TR/vocab-dcat-2/>), the W3C Open Digital Rights Language (<https://www.w3.org/TR/odrl-model/>), and the International Data Spaces Information Model (<https://w3id.org/idsa/core>)

## Identity and Trust

Identities, which are used to gain access to the Ecosystem, rely on
unique Identifiers and a list of attributes. Gaia-X uses existing Identities and does not maintain them directly. Uniqueness is ensured by
a specific Identifier format relying on properties of existing
protocols. The Identifiers are comparable in the raw form and should not 
contain more information than necessary (including Personal Identifiable Information). 
Trust -- confidence in the Identity and capabilities of a
Participant, Asset or Resource -- is established by cryptographically
verifying Identities using the Federated Trust Model of Gaia-X, which is
a component that guarantees identity proofing of the involved
Participants to make sure that Gaia-X Participants are who they claim to
be. In the context of Identity and Trust, the natural person or a
digital representation, which acts on behalf of a Participant is
referred to as Principal. As Participants need to trust other
Participants and Service Offerings provided, it is important that the
Gaia-X Federated Trust Model provides transparency for everyone.
Therefore, proper lifecycle management is required, covering Identity
onboarding, maintaining, and off-boarding. The table below shows the Participant
Lifecycle Process.

| Lifecycle Activity | Description |
|--------------------|-------------|
| Onboarding         | The governing body of a Gaia-X Ecosystem, represented by the Ecosystem's Federators, validates and signs the Self-Description provided by a Visitor (the future Participant/Principal). |
| Maintaining        | Trust related changes to the Self-Descriptions are recorded in a new version and validated and signed by the governing body of a Gaia-X Ecosystem. This includes both information controlled by the Participant/Principal. |
| Off-boarding        | The off-boarding process of a Participant is time-constrained and involves all dependent Participants/Principals. |

*Participant Lifecycle Process*

An Identity is composed of a unique Identifier and an attribute or set
of attributes that uniquely describe an entity (Participant/Asset)
within a given context. The lifetime of an Identifier is permanent. It may be used as a reference to an entity well beyond the lifetime of the entity it identifies or of any naming authority involved in the assignment of its name. Reuse of an Identifier for a different entity is forbidden. 
Attributes will be derived from existing identities as shown in the IAM Framework[^17].

[^17]: For a comprehensive view of the current discussion in the broader Gaia-X community, extra documents from the open working packages can be found on the Gaia-X community platform at <https://gaia.coyocloud.com/web/public-link/e01b9066-3823-42a7-b10b-9596871059ef/download>.

A secure Identifier for an Identity will be assigned by the issuer in a
cryptographically secure manner. This implies that Gaia-X Participants can self-issue Identifiers. It is solely the responsibility of a Participant to determine the conditions under which the Identifier will be issued.  Identifiers shall be derived from the native identifiers of an Identity System without any separate attribute needed. The Identifier shall provide a clear reference to the Identity System technology used.
In addition, the process of identifying
the holder of an Identity is transparent. Furthermore, it is traceable
since when the Identifier exists in this form, whether it is regularly
checked and according to which policy this is done. It must also be
possible to revoke issued Identity attributes[^18].

[^18]: For more on Secure Identities, see Plattform Industrie 4.0: Working Group on the Security of Networked Systems. (2016). Technical Overview: Secure Identities. <https://www.plattform-i40.de/PI40/Redaktion/EN/Downloads/Publikation/secure-identities.pdf>.

### Trust Framework

Gaia-X defines a technical trust framework based on open standards and under consideration of EU regulation, which is applicable 
for all Participants. The Trust Framework solution supports the privacy and
self-sovereign requirements and gains the chain of trust without the
need of a global and traceable unique ID across the Ecosystem.

Trust in Gaia-X is established by technical elements, such as technical
components and processes as well as by a fair and transparent governing
body.

The Gaia-X association AISBL is the main trust anchor. Participants trusting Gaia-X association AISBL
is a prerequisite for a functioning Ecosystem. In this sense, Gaia-X can
act as a Federator (according to section [Gaia-X Conceptual Model](conceptual_model.md)). Then, Gaia-X maintains a
list of organizations it trusts to carry out tasks like onboarding,
Certifications, and so forth. Participants are free to agree on
additional trust providing organizations, for example in certain
domains. The EU List of eIDAS Trusted Lists[^19] can also be used as a
source for trust service providers and Conformity Assessments Bodies.

[^19]: European Commission. Trusted List Browser: Tool to browse the national eIDAS Trusted Lists and the EU List of eIDAS Trusted Lists (LOTL). <https://webgate.ec.europa.eu/tl-browser/#/>

Self-Descriptions (see section [Federated Catalogue](federation_service.md#federated-catalogue)) play another crucial part in
establishing Trust within Gaia-X. In addition to non-trust-related
information, which can be updated by the Participant, they contain
trust-related information such as the organization DID and/or the
organization IDM OpenID Connect issuer (which connects to the
organization's Identity System). The trust-related part is vetted
according to Gaia-X Policy and electronically signed by a trusted
organization. Possible later changes regarding the trust related
information have to be approved. Gaia-X in turn maintains a
Self-Description, which lists its policies and accepted trust providers
as mentioned before.

Service Offerings may have different levels of Trust. During service
composition, it is determined by the lowest trust state of the Service
Offering it relies on. The trust state of a Service Offering will not
affect the trust state of a Participant. On the other hand, a Policy
violation of a Participant can result in losing the trust state of its
service.

### Hybrid Identity and Access Management

The Gaia-X IAM Framework supports two different approaches, the
federated identity approach and the decentralized identity approach.

In the federated identity approach, a Principal access a standardized
query API, which forwards the login request to the Gaia-X Internal
Access Management component (Gaia-X AM). The Gaia-X AM requests
authentication from the preselected Provider Identity System. The
Principal will provide the Credentials to the Identity System. The
Identity System validates the inputs and provides attributes to Gaia-X
AM, which grants / denies access to Gaia-X. Based on the assigned
Principal roles, specific permissions are granted or not.

In the decentralized identity approach, authentication and authorization
in Self Sovereign Identity (SSI) is based on decentralized identifiers
(DID)[^20]. Public key infrastructure is used to verify controllership
of a certain DID and Verifiable Credential[^21] that in turn contains
any kind of third party issued attributes. Those Verified Credentials
can be used to make decisions to grant access to certain Resources
(authorization). Depending on the existing system landscape, it may be
necessary to set up a "trusted transformation" point to translate
between new SSIs and existing Identity Systems. This outsources the
issuing and verification of Verifiable Credentials to another component,
controlled by an existing Identity System.

[^20]: W3C. (2021). Decentralized Identifiers (DIDs) v1.0. <https://www.w3.org/TR/did-core/>
[^21]: W3C. Verifiable Credentials Data Model 1.0: Expressing verifiable information on the Web [W3C Recommendation 19 November 2019]. <https://www.w3.org/TR/vc-data-model/>

Gaia-X might need to comply with additional requirements on the type and
usage of credential manager applications, e.g. mandatory
minimum-security requirements such as Multi Factor Authentication.
Server-to-Server Communication plays a crucial role in Gaia-X and the
integration of self-sovereignty will be worked out in more detail.

#### Federated Trust Model

For achieving Trust between identities, the Federated Trust Model is
built around the definition of standardized processes and practices,
incorporating general accepted policies as well as domain specific
policies derived out of private, industrial, governmental and
educational sectors.

![](media/image8.png)
*Detailed Level Design of the Gaia-X Federated Trust Model*

The Federated Trust Model achieves Trust between Consumers and
Providers. This is realized with the components shown in Figure 8. While
the Federated Trust Component and the Federated Catalogue have been
defined before, the Federated Trust Model further involves the Gaia-X
AM, which is an internal Gaia-X access management component responsible
for authorizing Principals' interactions within the Gaia-X Portals and
the Provider Access Management (Provider AM), which the Provider will
use to grant access for the Consumer to Service Instances.

Within the federated approach, Identities are built up of verifiable
Claims and shared on a need to know basis. For an operational example of
the Federated Trust Model, see [A2](appendix.md#a2).

### Access Control

The Access Management covers the internal Gaia-X AM and Provider AM. The
Provider AM in Gaia-X validates the Principal on the Consumer side using
the Federated Trust Component. End-Users are handled using existing
technology at the Consumer. For the Gaia-X AM, roles will be needed for
Gaia-X Principals which can be used for the access control. Examples for
such roles could be Gaia-X Administrator, Participant Administrator,
Principal. Roles will be maintained by the Gaia-X association AISBL. Clear policies
will be in place concerning processes and responsibilities[^22]. 

[^22]: For details, see chapter 2.1 in Gaia-X IAM Community Working Group. For a comprehensive view of the current discussion in the broader Gaia-X community, extra documents from the open working packages can be found on the Gaia-X community platform at <https://gaia.coyocloud.com/web/public-link/e01b9066-3823-42a7-b10b-9596871059ef/download>.

Gaia-X itself enables fine-grained access control-based attribute
evaluation. Attributes will be derived from metadata, Self-Descriptions
and runtime context (e.g. user Identity and associated properties).

Gaia-X will not implement central access control mechanisms for Assets
or Resources. The responsibility stays with the Provider. However,
Gaia-X will provide a standardized query API which enables the Provider
and Consumer to query and verify the Identity and Self-Description of
the respective other party. 

## Data Sovereignty Services

Data Sovereignty Services provide Participants the capability to be
entirely self-determined regarding the exchange and sharing of their
data. They can also decide to act without having the Data Sovereignty
Service involved, if they wish to do so.

Informational self-determination for all Participants comprises two
aspects within the Data Ecosystem: Transparency of data usages and
control of data usages. Enabling Data Sovereignty when exchanging,
sharing and using data relies on fundamental functions and capabilities
that are provided by Federation Services in conjunction with other
mechanisms, concepts, and standards. The Data Sovereignty Services build
on existing concepts of usage control that extends traditional access
control. Thus, usage control is concerned with requirements that pertain
to future data usages (i.e., obligations), rather than data access
(provisions).

### Capabilities for Data Sovereignty Services

The foundation for Data Sovereignty is a trust-management mechanism to
enable a reliable foundation for peer-to-peer data exchange and usage,
but also to enable data value chains with multiple Providers and
Consumers being involved. All functions and capabilities can be extended
and configured based on domain-specific or use case-specific
requirements in order to form reusable schemes.

The following are essential capabilities for Data Sovereignty in the
Gaia-X Data Ecosystems:

| Capability                                        | Description |
|---------------------------------------------------|-------------|
| Expression of Policies in a machine-readable form | To enable transparency and control of data usages, it is important to have a common policy specification language to express data usage restrictions in a formal and technology-independent manner that is agreed and understood within all Gaia-X Participants. Therefore, they have to be formalized and expressed in a common standard, e.g. ODRL[^23]. |
| Inclusion of Policies in Self-Description         | Informational self-determination and transparency require metadata to describe Data Assets including, Provider, Consumer, and Usage Policies as provided by Self-Descriptions and the Federated Catalogues. |
| Interpretation of Usage Policies                  | For a Policy to be agreed upon, it must be understood by all Participants in a way that enables negotiation and possible technical and organizational enforcement of Policies. |
| Enforcement                                       | Monitoring of data usages is a detective enforcement of data usages with subsequent (compensating) actions. In contrast, preventive enforcement[^24] ensures the policy Compliance with technical means (e.g., cancel or modify data flow). |

*Capabilities for Gaia-X Data Sovereignty Services*

[^23]: W3C. ODRL Information Model 2.2 [W3C Recommendation 15 February 2018]. <https://www.w3.org/TR/odrl-model/>
[^24]: Currently not in scope of Gaia-X Federation Services

### Functions of Data Sovereignty Services

Information services provide more detailed information about the general
context of the data usage transaction. All that information on the data
exchange and data usage must be traceable, therefore agreed monitoring
and logging capabilities are required for all data usages and
transactions. Self-determination also means that Providers can choose to
apply no Usage Policies at all.

The Data Sovereignty Services in Gaia-X implement different functions
for different phases of the data exchange. Therefore, three phases of
the data exchange have to be differentiated:

-   before transaction
-   during transaction
-   after transaction

Before the data exchange transaction, the Data Agreement Service is
triggered and both parties negotiate a data exchange agreement. This
includes Usage Policies and the required measures to implement those.
During the transaction, a Data Logging Service receives logging-messages
that are useful to trace the transaction. This includes data provided,
data received, policy enforced, and policy-violating messages. During
and after the transaction the information stored can be queried by the
transaction partners and a third eligible party, if required. The figure below
shows the role of mentioned services to enable sovereign data exchange.

![](media/image9.png)
*Data Sovereignty Services Big Picture*

The Data Agreement Service enables data transactions in a secure,
trusted, and auditable way. It offers interfaces for the negotiation
detailing the agreed terms for planned data exchange. The service is not
meant to handle the transaction of data (that is described in the
negotiated data contracts).

The Data Logging Service provides evidence that data has been (a)
submitted, (b) received and (c) that rules and obligations (Usage
Policies) were enforced or violated. This supports the clearing of
operational issues but also fraudulent transactions.

The Provider can track if, how, what data was provided, with the
Consumer being notified about this. The Consumer can track if data was
received or not, and, additionally, track and provide evidence on the
enforcement or violation of Usage Policies.

## Compliance

Gaia-X defines a Compliance framework that manifests e.g. in the form of
a code of conduct, third party Certifications / attestations, or
acceptance of terms and conditions. It is detailed in the Policy Rules document. 
Requirements from the field of security (e.g. data
encryption, protection, or interoperability) form the basis for this
Compliance framework. The main objective of the Federation Service
Compliance is to provide Gaia-X users with transparency on the
Compliance of each Service Offering.

This Federation Service consists of two components: First, the
Onboarding and Accreditation Workflow (OAW) that ensures that all
Participants, Assets, Resources and Service Offerings undergo a
validation process before being added to a Catalogue. Second, the
Continuous Automated Monitoring (CAM) that enables monitoring of the
Compliance based on Self-Descriptions. This is achieved by automatically
interacting with the service-under-test, using standardised protocols
and interfaces to retrieve technical evidence. One goal of the OAW is to
document the validation process and the generation of an audit trail to
guarantee adherence to generally accepted practices in Conformity
Assessment. Besides the general onboarding workflow, a special focus is
set on :

-   Monitoring of the Compliance relevant basis
-   Update of the Service Offering that imply re-assurance of Compliance
-   Suspension of Service Offerings
-   Revocation of Service Offerings

## Gaia-X Portals and API

The Gaia-X Portals support Participants to interact with 
Federation Service functions via a user interface, which provides
mechanisms to interact with core capabilities using API calls. The goal
is a consistent user experience for all tasks that can be performed with
a specific focus on security and Compliance. The Portals provide
information on Assets, Resources and Service Offerings and interaction
mechanisms for tasks related to their maintenance. Each Ecosystem can 
deploy its own Portals to support interaction with Federation Services. 
The functions of the Portals are further described below.

A Portal supports the registration of organizations as new Participants.
This process provides the steps to identify and authorize becoming a
Participant. Additionally, organizations are assisted singing up as
members of Gaia-X association AISBL. Participants are supported managing
Self-Descriptions and organizing Credentials. This includes
Self-Description editing and administration. A Portal further offers
search and filtering of Service Offerings and Participants, based on
Federated Catalogues. Additionally, solution packaging refers to a
composition mechanism for the selection and combination of Service
Offerings into solution packages to address specific use cases possible
with a Portal. To orchestrate the various APIs, an API framework to
create a consistent user and developer experience for API access and
lifecycle is introduced. An API gateway will ensure security to all
integrated services. An API portal will provide a single point of
information about available API services and version management. 
