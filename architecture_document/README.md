# GAIA-X Architecture Document

## Publisher

Gaia-X European Association for Data and Cloud AISBL  
Rue Royale 94  
1000 Bruxelles  
www.gaia-x.eu

## Authors

Gaia-X Technical Committee  
Gaia-X Work Packages  
Gaia-X Working Group Architecture  
Gaia-X Working Group Federation Services / Open Source Software  
Gaia-X Working Group Portfolio  
Gaia-X Working Group Provider  
Gaia-X Working Group User
Gaia-X Working Group X-Assocation

## Contact

E-mail: architecture-document@gaia-x.eu

## Copyright notice

©2021 Gaia-X European Association for Data and Cloud AISBL

This document is protected by copyright law and international treaties. You may download, print or electronically view this document for your personal
or internal company (or company equivalent) use. You are not permitted to adapt, modify, republish, print, download, post or otherwise reproduce or
transmit this document, or any part of it, for a commercial purpose without the prior written permission of Gaia-X European Association for Data and
Cloud AISBL. No copying, distribution, or use other than as expressly provided herein is authorized by implication, estoppel or otherwise. All rights not
expressly granted are reserved.

Third party material or references are cited in this document.

<script src="https://cdn.jsdelivr.net/npm/mermaid/dist/mermaid.min.js"></script>
<script>
function fix2124(id) {
    // fix issue https://github.com/mermaid-js/mermaid/issues/2124
    document.querySelectorAll("#" + id + " marker[markerWidth]").forEach((elt) => {
        elt.setAttribute("markerWidth", elt.getAttribute("markerWidth") > 100 ? elt.getAttribute("markerWidth")/10 : elt.getAttribute("markerWidth"))
    });
    document.querySelectorAll("#" + id + " marker[markerHeight]").forEach((elt) => {
        elt.setAttribute("markerHeight", elt.getAttribute("markerHeight") > 100 ? elt.getAttribute("markerHeight")/10 : elt.getAttribute("markerHeight"))
    });
}

mermaid.initialize({
    startOnLoad: true,
    mermaid: {
        callback: function(id) {
            console.log("mermaid callback with id", id);
            let svg = document.querySelector('#' + id);
            fix2124(id);
            let data = new XMLSerializer().serializeToString(svg);
            let blob = window.btoa(data);
            let img = new Image();
            img.onload = function() {
                let canvas = document.createElement('canvas');
                // svg.parentNode.appendChild(canvas);
                canvas.height = svg.getBoundingClientRect().height;
                canvas.width = svg.getBoundingClientRect().width;
                let ctx = canvas.getContext('2d');
                ctx.mozImageSmoothingEnabled = false;
                ctx.oImageSmoothingEnabled = false;
                ctx.webkitImageSmoothingEnabled = false;
                ctx.msImageSmoothingEnabled = false;
                ctx.imageSmoothingEnabled = false;
                ctx.drawImage(img, 0, 0);
                let img2 = document.createElement('img');
                img2.src = canvas.toDataURL();
                svg.parentNode.appendChild(img2);
                svg.remove();
            }
            img.src = "data:image/svg+xml;base64," + blob;
            // svg.parentNode.appendChild(img);
        }
    }
});</script>
</script>
