## Portability

Portability describes the ability to move data or applications between two different services at a low cost and with minimal disruption.

### references
- adapted from ISO/IEC 19941:2017(en)
