## Interoperability

Interoperability is defined according to ISO/IEC 17788:2014 as the ability of two or more systems or applications to exchange information and to mutually use the information that has been exchanged.

### references
- ISO/IEC 17788:2014
