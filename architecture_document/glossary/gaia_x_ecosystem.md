## GAIA-X Ecosystem

The GAIA-X Ecosystem consists of the entirety of all individual ecosystems that use the GAIA-X Architecture and conform to GAIA-X requirements.

Several individual ecosystems may exist (e.g. Catena-X), that orchestrate themselves, use the GAIA-X Architecture and may or may not use the GAIA-X Federation Services open-source software.
