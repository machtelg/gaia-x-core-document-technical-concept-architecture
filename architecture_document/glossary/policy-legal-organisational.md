## Policy (legal)

A statement of objectives, rules, practices or regulations governing the activities of people within a certain context.

They are placed in the [Federation Service](#federation-services) of [Compliance](#compliance).

### references

- NISTIR 4734 02/01/92: NISTIR 4734
- see Policies in Federation Service Compliance
