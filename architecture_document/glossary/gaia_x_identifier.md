## GAIA-X Identifier

One unique attribute used to identify an entity within the GAIA-X context and following GAIA-X format.
