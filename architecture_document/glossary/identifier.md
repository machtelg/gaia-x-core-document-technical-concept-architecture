## Identifier

One or more attributes used to identify an entity within a context.

### references
- ITU-T Recommendation X1252, Baseline identity management terms and definitions
