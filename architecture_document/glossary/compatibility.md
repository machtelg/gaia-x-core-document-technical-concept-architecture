## Compatibility

### definition
Compatibility is defined according to ISO/IEC 25010:2011 as the degree to which a product, system or component can exchange information with other products, systems or components, and/or perform its required functions, while sharing the same hardware or software environment

### references
- ISO/IEC 25010:2011
