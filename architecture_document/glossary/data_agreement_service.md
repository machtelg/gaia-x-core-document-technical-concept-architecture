## Data Agreement Service

The Data Agreement Service is a Federation Service of the category Data Sovereignty and considers negotiation of agreements for data exchange.

### alias
- DAS


### references
- Federation Services Specification GXFS
