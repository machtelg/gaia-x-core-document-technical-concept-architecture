## End-User

A natural person or process not being Principal, using digital offering from a [Consumer](#consumer). End-Users own an identity within the [Consumer](#consumer) context.

### references
- <https://www.iso.org/obp/ui/#iso:std:iso-iec:17788:ed-1:v1:en:sec:3.2.11>
