ADR-003: Lifecycle of Self-Descriptions
=======================================

:adr-id: 003
:revnumber: 1.0 
:revdate: 05-11-2020
:status: accepted
:author: Catalogue WP
:stakeholder: Self-Description WP, Catalogue WP

Summary
-------

There are four possible states for the lifecycle of GAIA-X Self-Descriptions:
active, eol (end of life), deprecated and revoked.

Context
-------

GAIA-X Assets (Nodes, Services, etc.) and their Self-Descriptions can be updated
over time. Furthermore, there can be Self-Descriptions that are abandoned or
even revoked for containing false information. This needs to be explicitly
tracked to prevent the Catalogue and GAIA-X participants to work with outdated
Self-Descriptions.

The claims of the Self-Descriptions in JSON-LD format can carry cryptographic
proofs based on hash signatures. Furthermore, the hash of the JSON-LD file can
be used as an identifier to reference to the source of information from the
Catalogue. Hence the JSON-LD files are immutable and can only be replaced as a
whole. The state of the Self-Descriptions therefore needs to be tracked as
metadata outside of the JSON-LD files itself.

End of Life: The Self-Descriptions are the source information for the GAIA-X
Catalogue. In order for the Catalogue to contain only up-to-date information it
should be "self-cleaning" whereby outdated information is removed automatically.
This can be achieved by timeout dates attached to every Self-Description after
which they are end-of-life. It is recommended that the automatic timeout date of
Self-Descriptions is set rather low, e.g. 90 days. This has proven useful in the
context of TLS certificates [LetsEncrypt] where a frequent renewal forces
providers that automated update systems are put in place instead of infrequent
manual updates.

Deprecated: If two versions of GAIA-X Assets (Nodes, Services, Data etc.) are
offered at the same time, then they each have independent Self-Descriptions. In
order for the Self-Descriptions to be self-contained, there shall be no partial
updates to Self-Descriptions. The entire JSON-LD file is published in an updated
version and the old Self-Description is deprecated with reference to the updated
Self-Description.

Revoked: GAIA-X needs to protect against bad actors that might not be able or
willing to correct false information. Hence a revocation mechanism is put into
place by which Self-Descriptions can be marked as non-active. Revocation can be
performed by the original issuer of a Self-Description and also by trusted
parties.

Decision Statements
-------------------

The Self-Descriptions in JSON-LD format (ADR-001) have an additional state
information that can change over time. The possible states are:

- active
- eol (end of life after a timeout date)
- deprecated (by a newer Self-Description)
- revoked (by a trusted party)

The default state is "active". The other states are terminal, so no further
state transitions are made once a Self-Description has become non-active.

Non-active Self-Descriptions might still be available in the JSON-LD format in
the historical record. But they are no longer considered for new search queries
in the Catalogue, etc.

Self-Descriptions have a timeout date after which they are in the "eol" state.
The timeout date is part of the JSON-LD file and considered in cryptographic
signatures.

Self-Descriptions in the JSON-LD format are immutable and cannot be modified
after they have been published. They can only be replaced by a new
Self-Description (deprecated) or given another non-active status.

A Self-Description with wrong or even fraudulent information can be revoked by
the original issuer or a trusted party. Who is allowed to revoke is at the
digression of the operators of the different Catalogues. Future rules for
Catalogue operators by the GAIA-X organization may apply.

Consequences
------------

Having decided on the possible states reveals new questions that need to be
answered in the context of the GAIA-X Catalogue.

The specification of the Catalogue has to describe in detail how the information
of the Self-Description state is exposed in its API and search query interfaces.

The state information needs to be distributed between Catalogue instances. There
might be disagreements / information gaps between Catalogue instances that need
to be resolved.

ADR References
--------------

- ADR-001: JSON-LD as the Exchange Format for Self-Descriptions

External References
-------------------

- [LetsEncrypt] https://letsencrypt.org
